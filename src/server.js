/* eslint-disable no-console */
const express = require('express'); //used for routing
const bodyParser = require('body-parser'); //to allow us to receive JSON
const Todo = require('./Todo');

const app = express();
app.use('/static', express.static('images')); //serve images from public folder using a virtual path prefix
app.use(bodyParser.json());

//ensure that the API is never cached in the browser.
app.use((req, res, next) => {
  res.setHeader('cache-control', 'private, max-age=0, no-cache, no-store, must-revalidate');
  res.setHeader('expires', '0');
  res.setHeader('pragma', 'no-cache');
  next();
});

//endpoints
app.get('/', (req, res) => res.send('Welcome'));
app.get('/todos', (_, res) => {
  Todo.findAll().then((todos) => {
    res.send(todos);
  });
});
app.post('/todos', (req, res) => {
  Todo.create({ note: req.body.note })
    .then((todo) => {
      res.send(todo);
    });
});
app.delete('/todos/:id', (req, res) => {
  Todo.findById(req.params.id)
    .then(todo => todo.destroy())
    .then(() => res.send());
});

app.listen(3000, () => console.log('Example app listening on port 3000!'));
